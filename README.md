# Getting Started with React App & GitLab CI/CD for S3 deployment

A CI/CD pipeline automates steps in the SDLC like builds, tests, and deployments. 

## setting up S3 bucket
*    Create a S3 bucket with public access.
    -   untick the `Block all public access` .

*   Enable Static web hosting
    -   Move to `properties` tab 
    -   scroll to bottom and enable `Static website hosting`.
    -   set index document as `index.html`

*   Setup the bucket policy (Permission tab) that allows everyone to read the files in the bucket.
    - Allow only GetObject to the users, not PUT, DELETE etc.


```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::Bucket-Name/*"
            ]
        }
    ]
}

```
## Setting UP IAM User for CI/CD

*   Create a user with Programtic access and S3 Permissions .

    -   following policy only allow put get list on `Bucket-Name` we specify.

```
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Effect":"Allow",
         "Action":[
            "s3:ListBucket"
         ],
         "Resource":"arn:aws:s3:::Bucket-Name"
      },
      {
         "Effect":"Allow",
         "Action":[
            "s3:PutObject",
            "s3:GetObject"
         ],
         "Resource":"arn:aws:s3:::Bucket-Name/*"
      }
   ]
}


```

*   Setup followingcreds in gitlab  `settings -> ci/cd -> variables`


* `AWS_ACCESS_KEY_ID` 
* `AWS_SECRET_ACCESS_KEY`
* `AWS_S3_BUCKET`

## Create the App
*   `npx create-react-app sample-react-app`

## create a pipeline as code
*    execute `touch .gitlab-ci.yml` in root directory.

## gitlab CI template
*   Avilable [here](.gitlab-ci.yml)


website - `bucketname.s3-website-us-east-1.amazonaws.com`
